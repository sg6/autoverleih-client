/* jQuery function - will be called on site load */
$(function() {

    /* variable definition */
    var _includedKm = 100;
    var _pricePerAdditionalKm = 0.19;
    var _pricePerDay = 49;
    var _from;
    var _to;
    var _url = '/FHAutoVerleih/webresources/fhautoverleih';
    var _price = 0.0;
    var _availableKm = 0;
    var x2js = new X2JS();
    
    getCurrencies();

    /* jQuery UI datepicker - see "gewünschter zeitraum" */
    $( "#inpFrom" ).datepicker({
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 2,
      dateFormat: 'yy-mm-dd',
      minDate: +1,
      onClose: function( selectedDate ) {
        $( "#inpTo" ).datepicker("option", "minDate", selectedDate );
      }
    });
    $( "#inpTo" ).datepicker({
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 2,
      dateFormat: 'yy-mm-dd',
      minDate: +1,
      onClose: function( selectedDate ) {
        $( "#inpFrom" ).datepicker( "option", "maxDate", selectedDate );
      }
    });

    /* listener for datepickers */
    $('#inpTo, #inpFrom').on('change', function() { 
        if($('#inpTo').val().length > 1 && $('#inpFrom').val().length > 1) {
            getCars(0,100);
            dateSet = true;
        }
        else {
            emptyCars();
        }
    });

    /* listener for submit button */
    $('#submit').on('click', function() {
        var j = {};
        j.id = 0;
        j.firstname = $('#inpFirstname').val();
        j.lastname = $('#inpLastname').val();
        j.address = $('#inpAddress').val();
        j.fromDate = $('#inpFrom').val();
        j.toDate = $('#inpTo').val();
        j.countKm = _distance;
        j.active = 1;
        j.fkCarId = $('#selCars').val();
        var s = JSON.stringify(j);
        $.ajax({
                url : _url + '.bookings',
                data : s,
                method: 'POST',
                dataType: 'json',
                contentType: 'application/json'
        }).done(function(d) {
            console.log(d);
            $('#msg').html('<h2>Buchung erfolgreich</h2>');
            $('#msg').append('<p>Buchung fehlerhaft?\n\
                <span id="storno">Buchung stornieren</span>!'
        );
        });
        setTimeout(function() {
            $.ajax({
               url : _url + '.bookings',
               data : {},
               method: 'GET',
               dataType: 'json'
            }).done(function(d) {
                var _last = 0;
                $.each(d, function(i, v) {
                   if(v.id > _last) {
                       _last = v.id;
                   }
                });
                $('#storno').attr('data-id', _last);
            });
        }, 500);
        
    });
    
    /* listener for remove button */ 
    $('body').on('click', '#storno', function() {
        $.ajax({
           url : _url + '.bookings/'+ (parseInt($('#storno').attr('data-id'))),
           method : 'DELETE'
        }).done(function(d) {
           alert('Erfolgreich storniert');
           window.location.reload();
        });
    });
    
    /* listener for distance calc button */
    $('#calcDistance').on('click', function() {
        calculateDistances();
    })
    
    $('#currencies').change(function() {
        getCurrencyRate("USD", $(this).val());
    });
    
    
    /* function: get all available cars */
    function getCars(from, to) {
        $.ajax({
                url : _url + '.cars',
                data : {},
                method: 'GET',
                dataType: 'json'
        }).done(function(d) {
            $('#selCars > option').remove();
            if(d != null && d != undefined && d.length > 0) {
                $.each(d, function(i,v) {
                    $('#selCars').append('<option value="'+v.id+'">'+ v.name + ' '+ v.type +'</option>');
                });
            }
        });
    }

    /* delete all existing cars */
    function emptyCars() {
        $('#selCars > option').remove();
        $('#selCars').append('<option>--- Bitte zuerst Datum auswählen ---</option>');
    }
    
    /* calculate price - business logic ! will be moved to service! */
    function getPrice() {
        if(!dateSet) {
            $('#output').html('Datumseingaben fehlerhaft');
        }
        else {
            _from = moment($('#inpFrom').val());
            _to = moment($('#inpTo').val());
            _days = _to.diff(_from, 'days') + 1;

            if(_days > 0) {
                $.ajax({
                   url : _url + ".bookings/getPrice/" + _days + "/" + _distance
                }).done(function(d) {
                    _price = parseFloat(d);
                    // todo get price in another currency
                    $('#output').html(_distance + " km in " + _days + " Tagen.<br />");
                    $('#output').append('Gesamtpreis*: USD ' + _price.toFixed(2) + '<br />');
                    if(_rate != 0.0) {
                        $('#output').append('Gesamtpreis*: '+$('#currencies').val()+' ' + (_price*_rate).toFixed(2));
                    }
                    $('#output').append('<p><small>*Schätzung auf Basis Ihrer Eingaben. Angaben ohne Gewähr</small></p>');
                });
            }
        }
    }
    
    function getCurrencies() {
        $.ajax({
            url : _url + ".bookings/currencies"
        }).done(function(d) {
            var j = JSON.stringify(x2js.xml_str2json(d));
            j = ($.parseJSON(j));
            $.each(j["Envelope"]["Body"]["getAllCurrenciesResponse"]["getAllCurrenciesResult"]["body"]["currencies"]["currency"],
                function(i, v) {
                    $('#currencies').append('<option value="'+v+'">'+v+'</option>');
                }
            );
            getCurrencyRate("USD", "EUR");
        });
    }
    
    function getCurrencyRate(cur1, cur2) {
        var r = 0.0;
        $.ajax({
            url : _url + ".bookings/currency/"+cur1+"/"+cur2,
        }).done(function(d) {
          var s = JSON.stringify(x2js.xml_str2json(d));
          var j = $.parseJSON(s);
          r = j.Envelope.Body.getCurrencyRateResponse.getCurrencyRateResult.body.ExchangeCurrency.rate;
          r = parseFloat(r);
          _rate = r;
        });
    }
    
    
    /***** MAPS *****/
    
    /* code from google */
    var map;
    var geocoder;
    var bounds = new google.maps.LatLngBounds();

    var origin2 = ''; //document.getElementById('inp1').value;
    var destinationA = ''; //document.getElementById('inp2').value;

    var destinationIcon = 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=D|FF0000|000000';
    var originIcon = 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=O|FFFF00|000000';

    function initialize() {
      var opts = {
      };
      geocoder = new google.maps.Geocoder();
    }

    function calculateDistances() {
      origin2 = document.getElementById('inp1').value;
      destinationA = document.getElementById('inp2').value;
      var service = new google.maps.DistanceMatrixService();
      service.getDistanceMatrix(
        {
          origins: [origin2],
          destinations: [destinationA],
          travelMode: google.maps.TravelMode.DRIVING,
          unitSystem: google.maps.UnitSystem.METRIC,
          avoidHighways: false,
          avoidTolls: false
        }, callback);
    }

    function callback(response, status) {
      if (status != google.maps.DistanceMatrixStatus.OK) {
        alert('Error was: ' + status);
      } else {
        var origins = response.originAddresses;
        var distance = response.rows[0].elements[0].distance.value;
        var destinations = response.destinationAddresses;
        var outputDiv = document.getElementById('outputDiv');
        _distance = parseInt(distance / 1000);
        getPrice();
      }
    }

    google.maps.event.addDomListener(window, 'load', initialize);


});